const BOOKS_PATH = "./images/books";
const borrowedBooks = JSON.parse(localStorage.getItem("borrowedBooks") || "[]");

const overlayBackground = document.getElementById("overlay-background");
const overlayContent = document.getElementById("overlay-content");
const overlayClose = document.getElementById("overlay-close");
const overlayKeepBorrowing = document.getElementById("overlay-keep-borrowing");
const overlayInnerWrapper = document.querySelector("#overlay-inner-wrapper")

const booksOverviewButton = document.getElementById("books-overview-button");
const overlayBookList = document.querySelector("#overlay-book-list");

const booksWrapper = document.querySelector("#books-wrapper");
const borrowedBooksCounter = document.querySelector("#borrowed-books-counter");
const returnAllButton = document.getElementById("returnAll");



let books = {
    "Polarsirkelen": {
        img: "polarsirkelen.jpeg",
        name: "Polarsirkelen",
    },
    "USA": {
        img: "usa.png",
        name: "USA",
    },
    "Kniv": {
        img: "kniv.jpeg",
        name: "Kniv",
    },
    "To-Søstre": {
        img: "tosøstre.jpeg",
        name: "To Søstre",
    },
    "All-Your-Perfects": {
        img: "allyourperfects.jpeg",
        name: "All Your Perfects",
    },
    "Dune": {
        img: "dune.jpeg",
        name: "Dune",
    },
    "The-Last-Wish": {
        img: "thelastwish.jpeg",
        name: "The Last Wish",
    },
    "A-Good-Girls": {
        img: "agoodgirls.jpeg",
        name: "A Good Girls",
    },
    "As-Good-As-Dead": {
        img: "asgoodasdead.jpeg",
        name: "As Good As Dead",
    },
    "Den-Andre": {
        img: "Denandre.jpeg",
        name: "Den Andre",
    },
    "Drømmen-Om-Et-Tre": {
        img: "drømmenOmEtTre.jpeg",
        name: "Drømmen Om Et Tre",
    },
    "Gjestene": {
        img: "gjestene.jpeg",
        name: "Gjestene",
    },
    "Havets-Kirkegård": {
        img: "havetskirkegaard.jpeg",
        name: "Havets Kirkegård",
    },
    "Heartstopper": {
        img: "heartstopper.jpeg",
        name: "Heartstopper",
    },
    "Jul-i-Nyhavn": {
        img: "JuliNyhavn.jpeg",
        name: "Jul i Nyhavn",
    },
    "Shatter-Me": {
        img: "shatterme.jpeg",
        name: "Shatter Me",
    },
    "Six-of-Crows": {
        img: "sixofcrows.jpeg",
        name: "Six of Crows",
    },
    "Snømannen": {
        img: "snømannen.jpg",
        name: "Snømannen",
    },
    "The-Midnight-Library": {
        img: "themidnightlibrary.jpeg",
        name: "The Midnight Library",
    },
    "The-Secret-History": {
        img: "thesecrethistory.jpeg",
        name: "The Secret History",
    },
    "Twilight": {
        img: "twilight.jpeg",
        name: "Twilight",
    },
}


function updateOverlayBookList() {
    overlayBookList.innerHTML = "";

    if (borrowedBooks.length === 0) {
        overlayBookList.innerHTML = "Ingen bøker er utlånt";
        return;
    }

    borrowedBooks.forEach(book => {
        const li = document.createElement("li");
        li.innerText = books[book].name;
        overlayBookList.appendChild(li);
    });
}


function resizeOverlay() {
    const height = Math.min(overlayInnerWrapper.offsetHeight, window.innerHeight - 200);
    overlayContent.style.height = `${height}px`;
}


function toggleOverlay() {
    updateOverlayBookList();

    overlayBackground.classList.toggle("active-block");
    overlayContent.classList.toggle("active-block");

    resizeOverlay();
}


function setBorrowedBooksCount() {
    borrowedBooksCounter.innerText = borrowedBooks.length;
}


function setBorrowedBooks() {
    localStorage.setItem("borrowedBooks", JSON.stringify(borrowedBooks));
}


function borrowBookInternal(book) {
    // Shouldn't be possible since the button is disabled therefore an error doesn't
    // need to be shown.
    if (borrowedBooks.includes(book)) return;
    
    borrowedBooks.push(book);
    setBorrowedBooks();
}


function returnBookInternal(book) {
    // Shouldn't be possible since the button is disabled therefore an error doesn't
    // need to be shown.
    if (!borrowedBooks.includes(book)) return;

    borrowedBooks.splice(borrowedBooks.indexOf(book), 1);
    setBorrowedBooks();
}


function borrowBook(parent) {
    const book = parent.id;
    borrowBookInternal(book);

    const borrowButton = parent.querySelector(".borrow-button");
    const returnButton = parent.querySelector(".return-button");

    borrowButton.classList.add("disabled");
    returnButton.classList.remove("disabled");
    setBorrowedBooksCount();
}


function returnBook(parent) {
    const book = parent.id;
    returnBookInternal(book);

    const borrowButton = parent.querySelector(".borrow-button");
    const returnButton = parent.querySelector(".return-button");

    borrowButton.classList.remove("disabled");
    returnButton.classList.add("disabled");
    setBorrowedBooksCount();
}


function setupBook(book, isBorrowed = false) {
    const outerDiv = document.createElement("div");
    outerDiv.classList.add("book");
    outerDiv.setAttribute("id", book);

    const h3 = document.createElement("h3");
    h3.innerText = books[book].name;
    outerDiv.appendChild(h3);

    const img = document.createElement("img");
    img.setAttribute("src", `${BOOKS_PATH}/${books[book].img}`);
    outerDiv.appendChild(img);

    const innerDiv = document.createElement("div");
    innerDiv.classList.add("button-wrapper");
    outerDiv.appendChild(innerDiv);

    const borrowButton = document.createElement("button");
    borrowButton.innerText = "Lån";
    borrowButton.classList.add("borrow-button");

    const returnButton = document.createElement("button");
    returnButton.innerText = "Lever";
    returnButton.classList.add("return-button");
    returnButton.classList.add("return");

    if (!isBorrowed) {
        returnButton.classList.add("disabled");
    } else {
        borrowButton.classList.add("disabled");
    }

    innerDiv.appendChild(borrowButton);
    innerDiv.appendChild(returnButton);

    borrowButton.addEventListener("click", function() {
        borrowBook(outerDiv);
        toggleOverlay();
    });
    returnButton.addEventListener("click", function() {
        returnBook(outerDiv);
    });

    booksWrapper.appendChild(outerDiv);
}

function returnAllBooks () {
    // Need to use a copy since the array is modified in the loop
    const borrowedBooksCopy = [...borrowedBooks];

    borrowedBooksCopy.forEach(book => {
        const element = booksWrapper.querySelector(`#${book}`);
        returnBook(element)
    });

    updateOverlayBookList();
    resizeOverlay();
}


// Initialize books
Object.keys(books).forEach(book => setupBook(book, borrowedBooks.includes(book)));
setBorrowedBooksCount();


returnAllButton.addEventListener("click", returnAllBooks)
window.addEventListener("resize", resizeOverlay);
booksOverviewButton.addEventListener("click", toggleOverlay);
overlayBackground.addEventListener("click", toggleOverlay);
overlayClose.addEventListener("click", toggleOverlay);
overlayKeepBorrowing.addEventListener("click", toggleOverlay);
