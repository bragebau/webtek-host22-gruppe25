const btn = document.getElementById("buttonContact");
const inpt = document.getElementById("inputContact");
const form = document.getElementById("emailForm");

function validateEmail(emailInput) {
    var validInput = /^[\w\-\.]+@(?:[\w-]+\.)+[\w-]{2,4}$/;
    if (emailInput.match(validInput)) {
        alert("Valid email address!");

        document.form.focus();

        return true;
    } else {
        alert("Invalid email address!");

        document.form.focus();

        return false;
    }
}

form.addEventListener("submit", (event) => {
    event.preventDefault();
    validateEmail(inpt.value);
});
