const åpningstider = document.getElementById("Åpningstider");

const curr = new Date(Date.now());
const first = curr.getDate() - curr.getDay() + 1;

function getActualDate(date) {
    return new Date(curr.setDate(date));
}

const dates = [getActualDate(first)];

for(let i=0; i<=6; i++){
    dates.push(getActualDate(first+i+1));
}

function readabledate(date){
    return date.getDate()+"."+(date.getMonth() + 1)+"."+ date.getFullYear();
}


const table = åpningstider.querySelector("tbody");

for (let i=0; i<table.children.length; i++) {
    if (i == 0) continue;

    const child = table.children[i];
    const tr = child.children[1];
    tr.innerHTML = readabledate(dates[i-1]);
}
