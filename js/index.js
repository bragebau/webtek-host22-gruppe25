// Using DOMContentLoaded instead of load because we want to inject
// the elements before the page is shown. This way we can avoid the
// page flickering.
window.addEventListener("DOMContentLoaded", function() {
    const bodyElem = document.querySelector("body");

    const headerElem = document.createElement('header');
    headerElem.innerHTML = `
        <div id="top-header">
            <div class="ntnu">
                <a href="https://www.ntnu.no/" target="_blank">
                    <img src="./images/ntnu2.png" alt="NTNU Logo">
                </a>
            </div>
            <div class="title">
                <a href="./home.html">Online Bibliotek</a>
            </div>
            <div class="online" >
                <a href="https://online.ntnu.no/" target="_blank">
                    <img src="./images/online2.png" alt="Online Logo">
                </a>
            </div>
        </div>

        <nav>
            <a href="./home.html">HJEM</a>
            <a href="./about.html">OM OSS</a>
            <a href="./contact.html">KONTAKT</a>
            <a href="./weekly.html">UKENS BOK</a>
            <a href="./borrow.html">LÅNE-SIDE</a>
        </nav>`
    bodyElem.prepend(headerElem);
    
    const footerElem = document.createElement('footer');
    footerElem.innerHTML = `
        <div id="outer-wrapper">
            <div id="left-wrapper">
                <div id="inner-wrapper-1">
                    <h1>KONTAKT</h1>
                </div>            
                        
                
                <div id="inner-wrapper-2">
                    <div id="container-1">
                        <p>Telefon:</p>
                        <p>Mail:</p>
                    </div>
                    <div id="container-2">    
                        <p>+47 68 8849</p>
                        <p>online.biblioteket@ntnu.no</p>
                    </div>
                </div>    
            </div>
            <div id="right-wrapper">
                <a href="https://use.mazemap.com/#v=1&zlevel=4&center=10.404813,63.415563&zoom=18&campusid=1&sharepoitype=poi&sharepoi=1000292427" target="_blank">
                    <img src="./images/testkart.png">
                </a>
            </div>
        </div>`
    bodyElem.append(footerElem);
});
